package culc

import (
	"fmt"

	"gitlab.com/k-terashima/casino-21/models"
)

// Point : カード配列を渡すと現ポイントを計算してくれる
func Point(c []models.Card) int {
	var (
		p int

		// ACEの出現と回数
		ace   bool
		times int
	)

	// カードを場合により計算する
	// check: [{104 HEART 1 false} {180 HEART 7 false} {151 SPADE 12 false} {54 DIAMOND 1 false} {11 SPADE 3 false}]
	for _, v := range c {
		// 10以上はすべて10として計上
		if 10 < v.Number {
			p += 10
		} else if v.Number == 1 {
			ace = true
			times++
		} else {
			p += v.Number
		}
	}

	// Aを持っている場合、点数に応じて数字を変える
	if ace != false {
		for i := 0; i < times; i++ {
			// Aがまだ残っているもブラックジャックになる場合
			if p == 21 && 0 < times {
				p++
			}

			if p < 11 {
				p += 11
			} else if p < 21 {
				// Aが２枚のときに、p=20に1追加するが、次に1追加する機会をもらえないままブラックジャック（有利判断）される
				p++
			}

		}
		fmt.Printf("----------------ACEが%d枚あるよ、有利な方で選択可能だよ\n", times)
	}

	// ブラックジャック判定
	if p == 21 {
		fmt.Println("ブラックジャックですやん！")
	}

	fmt.Print("\n")
	return p
}
