package models

// Card : 1枚単位のカード情報
// Aの場合は、ifで13or1で有利な方に振り分ける
type Card struct {
	// ランダムで配布する時
	ID int
	// 絵柄など
	Mark string
	// 基本数
	Number int
	// 配布済みか否か
	Done bool
}

// Deck : デッキ52枚
type Deck struct {
	Cards [52]Card
}

// UserInfo : 現在の持ち点などの総合的情報
type UserInfo struct {
	ID        int64
	AccountID string
	Password  string
	Money     int64
}

// UserCard : 現在のユーザ手札
type UserCard struct {
	Premium int64
	Cards   []Card
	Points  uint
	Burst   bool
}

// DealerInfo : 現在の持ち点などの総合的情報
type DealerInfo struct {
	// 総合損益
	Money int64
	// ゲーム回数
	Games int64
}

// DealerCard : 現在のディーラー手札
type DealerCard struct {
	Cards  []Card
	Points uint
	Burst  bool
}
