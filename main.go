package main

import (
	"fmt"
	"html/template"
	"io"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/k-terashima/casino-21/culc"
	"gitlab.com/k-terashima/casino-21/models"

	"github.com/labstack/echo"
)

var (
	// 初期デッキ
	decks = make([]models.Deck, models.DECKIES)
	// 配布済みカード
	dist []models.Card
)

func init() {
	t := time.Now()
	// トランプ1デッキを作る
	k := 0
	for i := range decks {
		for n := 0; n < 52; n++ {
			decks[i].Cards[n].ID = k
			k++
			switch {
			case n%4 == 0:
				decks[i].Cards[n].Mark = "HEART"
				a := n/4 + 1
				decks[i].Cards[n].Number = a
			case n%3 == 0:
				decks[i].Cards[n].Mark = "CLOVER"
				a := n/4 + 1
				decks[i].Cards[n].Number = a
			case n%2 == 0:
				decks[i].Cards[n].Mark = "DIAMOND"
				a := n/4 + 1
				decks[i].Cards[n].Number = a
			default:
				decks[i].Cards[n].Mark = "SPADE"
				a := n/4 + 1
				decks[i].Cards[n].Number = a
			}
		}
	}

	// // 検証とかあればいいよね
	// fmt.Println(decks)
	af := time.Now()
	fmt.Printf("デッキx%d作成時間: %v #Initialize\n\n", models.DECKIES, af.Sub(t))
}

// Template Render用
type Template struct {
	templates *template.Template
}

// Render renders a template document
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {
	// 配布用ランダムシードを作る
	sBase := time.Now().UnixNano()
	// sSqrt := math.Sqrt(float64(sBase))
	rand.Seed(int64(sBase))

	// プロットを作る
	var (
		// XYs    plotter.XYs
		user   models.UserCard
		dealer models.DealerCard
		card   models.Card
	)

	for i := 0; i < 2; i++ {
		card = dealCard()
		dist = append(dist, card)
		user.Cards = append(user.Cards, card)
		card = dealCard()
		dist = append(dist, card)
		dealer.Cards = append(dealer.Cards, card)
	}
	// 各人の点数
	pUser := culc.Point(user.Cards)
	pDealer := culc.Point(dealer.Cards)

	// 22以上ならば負け確定
	// 21なばらブラックジャック勝ち確定
	// どちらもブレイクしてディーラーの変化を待つ
	if pUser < 21 || pUser != 21 {
		// ユーザはStay/Hit/double/Splitを選択
		for {
			choose := chooseUser(pUser)
			// ステイの場合は再度カードを配る
			if choose == 1 {
				fmt.Printf("%d:Hitなのでカードを配布\n", choose)
				card = dealCard()
				dist = append(dist, card)
				user.Cards = append(user.Cards, card)
				pUser = culc.Point(user.Cards)

				// 22以上ならば負け確定
				// 21なばらブラックジャック勝ち確定
				// どちらもブレイクしてディーラーの変化を待つ
				if pUser > 21 || pUser == 21 {
					break
				}
			} else {
				break
			}
		}
	}

	// ディーラーは17以下ならばエンドレスドロー
	for {
		if pDealer < 18 {
			card = dealCard()
			dist = append(dist, card)
			dealer.Cards = append(dealer.Cards, card)
			pDealer = culc.Point(dealer.Cards)
		} else {
			break
		}
	}

	fmt.Println("配布カードUSER: ", pUser)
	fmt.Println("配布カードDEALER: ", pDealer)
	fmt.Print("\n")

	sBranch(pUser, pDealer)
	fmt.Println(user.Cards)
	fmt.Println(dealer.Cards)
	fmt.Print("\n")

	// XYs = views.PlotXYs(dist)
	// views.PlotData(XYs)

	// // Server Start
	// startServer()
}

// views : 表示系
func index(c echo.Context) error {
	return c.Render(http.StatusOK, "index", map[string]interface{}{
		"name": "Dolly!",
	})
}

// カードを配る
func dealCard() models.Card {
	var (
		c models.Card
	)
	for n := 0; ; n++ {
		// プレイヤーの数を定義する
		// まずは2極対立

		// 配る
		// r := rand.Intn(len(decks) * len(decks[0].Cards))
		d := rand.Intn(len(decks))
		a := rand.Intn(len(decks[0].Cards))

		c = decks[d].Cards[a]
		if c.Done != true {
			// // 全カードをIDで並べる
			// fmt.Println("配布用: ", r)
			// 乱数とIDの合致を図る
			decks[d].Cards[a].Done = true
			break
		}

		// fmt.Println("ランダム引数: ", r)
		// fmt.Println("どれ引く？: ", c)
		// fmt.Printf("なぜなら: decks[%d].Cards[%d]\n", h, a)
		// fmt.Println("配布済みCard: ", dist)
	}

	return c
}

func startServer() {
	// サーバー起動
	// 顧客への表示出力
	e := echo.New()
	temp := &Template{
		templates: template.Must(template.ParseGlob("_template/*.gohtml")),
	}
	e.Static("/", "./_static")
	e.Renderer = temp
	// Route
	e.GET("/", index)
	e.Logger.Fatal(e.Start(models.PORT))
}

// chooseUser 選択肢入力
func chooseUser(num int) int {
	var n int
	for {
		var scan int
		fmt.Printf("現在あなたの持ち点は%d\n選択肢を入力してください [0:Stay/1:Hit/2:Double/3:Split]\nNumber = ", num)
		fmt.Scan(&scan)
		switch {
		case scan == 0:
			fmt.Println("ユーザ: ステイ")
		case scan == 1:
			fmt.Println("ユーザ: ヒット")
		case scan == 2:
			fmt.Println("ユーザ: ダブル")
		case scan == 3:
			fmt.Println("ユーザ: スプリット")
		default:
			fmt.Println("0, 1, 2, 3 で記入ください")
		}

		// 可用な選択肢ならばforを抜ける
		if -1 < scan && scan < 4 {
			n = scan
			break
		}
	}

	fmt.Print("\n")
	return n
}

func sBranch(pUser, pDealer int) {
	if pUser == 21 {
		fmt.Printf("ﾌﾞﾗｰｯｸヽ(∀ﾟ )人(ﾟ∀ﾟ)人( ﾟ∀)ノｼﾞｬｰｰｯｸ━━━!!\n")
		fmt.Printf("%d: ブラックジャック！ユーザーの勝ちです\n", pUser)
		fmt.Print("\n")
	} else if pUser > 21 {
		fmt.Printf("∑(๑°口°ll๑)(・´з`・)(・´з`・)(・´з`・).。バースト\n")
		fmt.Printf("%d: ユーザーがバーストしました。ユーザーの負けです\n", pUser)
		fmt.Print("\n")
	} else if pDealer > 21 {
		fmt.Printf("ｷﾀヽ(∀ﾟ )\\( ¨̮ )/\\( ¨̮ )/\\( ¨̮ )/\\( ¨̮ )/いえい\n")
		fmt.Printf("%d: ディーラーがバーストしました。ユーザーの勝ちです\n", pDealer)
		fmt.Print("\n")
	} else {
		if pUser < pDealer {
			fmt.Printf("(๑°ㅁ°๑)(・´з`・)(・´з`・)(・´з`・).。ヒットすべきだった？\n")
			fmt.Printf("%d: ディーラーの勝ちです\n", pDealer)
			fmt.Print("\n")
		} else if pDealer < pUser {
			fmt.Printf("(๑•̀ㅂ•́)و✧(๑ ˙˘˙)/(๑ ˙˘˙)/(๑ ˙˘˙)/.。勝ちやん\n")
			fmt.Printf("%d: ユーザーの勝ちです\n", pUser)
			fmt.Print("\n")
		} else {
			fmt.Printf("・ω・・ω・・ω・・ω・.。掛け金没収…\n")
			fmt.Printf("ユーザー%d - ディーラー%dでドローです\n", pUser, pDealer)
			fmt.Print("\n")
		}
	}
}
